# Copyright (c) 2021 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Test cases for the boolfunc module"""
import unittest

from kpet import boolfunc


class BoolFuncTest(unittest.TestCase):
    """Test cases for the boolfunc module."""

    def test_minimize_and_format(self):
        """
        Check get_function(solve()) returns correct results, according to
        examples in the original code.
        """
        def minimize_and_format(ones, dont_care):
            variables = ['A', 'B']
            return boolfunc.minexpr_format(
                variables,
                boolfunc.minimize(len(variables), ones, dont_care)[1]
            )

        self.assertEqual('0', minimize_and_format([], []))
        self.assertEqual('1', minimize_and_format([1, 3], [0, 2]))
        self.assertEqual('1', minimize_and_format([0, 1, 2, 3], []))
        self.assertEqual('A&B', minimize_and_format([3], []))
        self.assertEqual('!A&!B', minimize_and_format([0], []))
        self.assertEqual('A', minimize_and_format([1, 3], []))
        self.assertEqual('A', minimize_and_format([1], [3]))
        self.assertEqual('B', minimize_and_format([2, 3], []))
        self.assertEqual('!A', minimize_and_format([0, 2], []))
        self.assertEqual('!B', minimize_and_format([0, 1], []))
        self.assertEqual('A | B', minimize_and_format([1, 2, 3], []))
        self.assertEqual('!B | !A', minimize_and_format([0, 1, 2], []))

    def test_calculate_complexity(self):
        """
        Check calculate_complexity() returns correct results, according to
        examples in the original code.
        """
        def calculate_complexity(*minterms):
            return boolfunc.calculate_complexity(3, minterms)

        self.assertEqual(0, calculate_complexity((1, 6)))
        self.assertEqual(1, calculate_complexity((0, 6)))
        self.assertEqual(2, calculate_complexity((3, 4)))
        self.assertEqual(3, calculate_complexity((7, 0)))
        self.assertEqual(3, calculate_complexity((1, 6), (2, 5), (4, 3)))
        self.assertEqual(4, calculate_complexity((0, 6), (2, 5), (4, 3)))
        self.assertEqual(5, calculate_complexity((0, 6), (0, 5), (4, 3)))
        self.assertEqual(6, calculate_complexity((0, 6), (0, 5), (0, 3)))
        self.assertEqual(10, calculate_complexity((3, 4), (7, 0), (5, 2)))
        self.assertEqual(11, calculate_complexity((1, 4), (7, 0), (5, 2)))
        self.assertEqual(11, calculate_complexity((2, 4), (7, 0), (5, 2)))
        self.assertEqual(12, calculate_complexity((0, 4), (7, 0), (5, 2)))
        self.assertEqual(15, calculate_complexity((0, 4), (0, 0), (5, 2)))
        self.assertEqual(17, calculate_complexity((0, 4), (0, 0), (0, 2)))
